const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//The auth.verify acts as a middleware to ensure that the user is logged in before they can enroll to a course.
router.get('/details', auth.verify, (req, res) => {

	//Use the "decode" method defined in the 'auth.js' file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})
module.exports = router;

