const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Check if the email already exists


/*
Steps:
1. Use mongoose "find" method to find the duplicate emails
2. Use the "then" method to send a respose back to the client based on the result of the find method(email already exists/does not exist);
*/
module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email }).then(result => {
		//The "find" method returns a record if a match is found
	if(result.length > 0){
		return true;
	}else{
		//No duplicate found
		
		return false;
	}
})

}

//User Registration
/*
1. Create a new User object usingn the mongoose model and the information from the request body.
2. Error handling, if error, return error, else save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User( {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//Saves the created object to our database
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//User authentication

/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne( {email: reqBody.email} ).then(result => {
		//If user does not exist
		if(result == null){
			return false;
		}else{
			//user exists
			//Create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			//"compareSync" method is used to compare is non encrypted password from the login form to the encrypted password retrieved
			//from the database and returns "true" or "false" value depending on the result
			//A good practice for boolean variable/constants is used, the word "is" or
			//ex. isSingle, isDone, areDone etc.
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//If the passwrods match/result of the above code is true
			if(isPasswordCorrect){
				//Generate an access token
				//Use the "createAccessToken" method defined in the 'auth.js' file
				//returning an object back to the frontend
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				//Passwords do not match
				return false;
			}

		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
			result.password = " ";
			return result;
	})
}