const Course = require('../models/Course');


//Creation of Course
/*
Steps:
1.  Create a conditional statement that will check if the user is an admin.
2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
3. Save the new Course to the database

*/

module.exports.addCourse = (data) => {
	//User is an admin
	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})
		console.log(data);
		return newCourse.save().then((course, error) => {
			//Course creation failed
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		//User is not an admin
		return false;
	}
}

//Retrieve All Courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//Retrieve all ACTIVE Courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

//Retrieve Speficic Course
module.exports.specificCourse = (courseId) =>{
	return Course.findById(courseId)
}


//Update a Course
//Steps:
/*
1. CHeck if the user is an admin
2. Create a variable which will contain the information retrieved from the req.body
3. Find and update the course using the courseId retrieved from the req.params
and the variable contraining the information from the requestt body.

*/
module.exports.updateCourse = (courseId, newUpdate, data) => {
	if(data.isAdmin){
	let newCourse = new Course({
		name: newUpdate.name,
		isActive: newUpdate.isActive,
		description: newUpdate.description,
		price: newUpdate.price
	})
	return Course.find(courseId).then(result => {
		return result = newCourse;
})
}
}